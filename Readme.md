![soffico Orchestra Logo](Images/orchestra.png)
# Monitoring soffico Orchestra with PRTG

You can monitor your soffico Orchestra installation with the EXE/Script Advanced sensor from PRTG and a custom script. With the custom script, you can create a custom Orchestra Channel sensor. Orchestra uses scenarios, which you will also need for the complete integration into PRTG. You can request a scenario from soffico directly.

For individual interfaces, the custom Orchestra Channel sensor monitors the number of successful and faulty messages per channel. The sensor makes it possible to monitor important parameters of the integration engine and thus to integrate a central and important part of your medical infrastructure in PRTG.

### Download

Please use this link to download [https://gitlab.com/PRTG/Sensor-Scripts/OrchestraSensor/-/jobs/artifacts/master/download?job=PRTGDistZip]

### Installation
- Deploy the following files to <prtg_folder>\custom sensors\EXEXML
 - log4net.dll
 - Paessler.Config.dll
 - Paessler.Log.dll
 - Paessler.Xml.dll
 - RestSharp.dll
 - sofficoOrchestraSensor.exe
- Deploy custom.orchestra.channelstate.ovl to <prtg_folder>\lookups\custom
- Deploy soffico Orchestra.odt to <prtg_folder>\devicetemplates

The following metrics can be monitored:
 
#### Orchestra Channel:
- AdapterState
- Error Count
- OK Count

```
-host=%host -port=8019 -type="channel" -channel="UID"
```

![Image of Orchestra Channel Sensor](Images/orchestra_channel_sensor.png)

# Auto-Discovery support via Template
  - Select "soffico Orchestra" template in Device Settings as Template
  - Run Auto-Discovery with specified template
  - Newly added channels from Orchestra will be added in PRTG if you schedule a reoccuring Auto-Discovery for this device
